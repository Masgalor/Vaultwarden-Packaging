#! /bin/bash

#Get Arguments
while getopts ":d:" opt; do
  case $opt in
    d) DIR="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done
if [ -z "$DIR" ]; then DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"; fi


#Check if we are running in a compatible working directory
cd "$DIR" || exit

if osc info 2> /dev/null | grep -q 'Package name: vaultwarden-webvault'; then
  isWebvault=true
elif osc info 2> /dev/null | grep -q 'Package name: vaultwarden'; then
  isVaultwarden=true
else
  echo "This does not look like a legit working directory."
  exit
fi


#Get the latest project- and compiler-version
revVaultwarden=$(curl -s https://api.github.com/repos/dani-garcia/vaultwarden/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 1-)
revWebvault=$(curl -s https://api.github.com/repos/dani-garcia/bw_web_builds/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 2-)
revRust=$(curl -s https://api.github.com/repos/rust-lang/rust/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 1-)

echo "The working-directory will be upgraded to:"
if [ "$isVaultwarden" = true ]; then
  echo "Vaultwarden $revVaultwarden"
  echo "Rust $revRust"
elif [ "$isWebvault" = true ]; then
  echo "Webvault $revWebvault"
fi
echo "Do you want to continue? (y/n)"
read -r userDecission
echo " "
if ! [ "$userDecission" = "y" ]; then
  exit
fi


#Prepare and cleanup
if [ -f "debian.tar.gz" ]; then
  [ -d ./debian ] && rm -rf ./debian
  tar -xf "debian.tar.gz"
  rm "debian.tar.gz"
fi
[ -f vendor.tar.xz ] && rm vendor.tar.xz
[ -f cargo_config ] && rm cargo_config


if [ "$isVaultwarden" = true ]; then
  #Upgrade Rust version
  sed -i 's#<param name="path">dist/rust-.*#<param name="path">dist/rust-'"$revRust"'-x86_64-unknown-linux-gnu.tar.gz</param>#' _service
  sed -i 's#Source2:   rust-.*#Source2:   rust-'"$revRust"'-x86_64-unknown-linux-gnu.tar.gz#' vaultwarden.spec
  sed -i 's#rust-.*#rust-'"$revRust"'-x86_64-unknown-linux-gnu.tar.gz#' vaultwarden.dsc
  sed -i 's#rust-.*#rust-'"$revRust"'-x86_64-unknown-linux-gnu.tar.gz#' debian/source/include-binaries
  #Upgrade Vaultwarden version
  sed -i 's#<param name="revision">.*#<param name="revision">'"$revVaultwarden"'</param>#' _service
  sed -i 's#DEBTRANSFORM-TAR: vaultwarden-.*#DEBTRANSFORM-TAR: vaultwarden-'"$revVaultwarden"'.tar.gz#' vaultwarden.dsc
  sed -i 's#export VW_VERSION=.*;#export VW_VERSION='"$revVaultwarden"';#' debian/rules
elif [ "$isWebvault" = true ]; then
  #Upgrade Webvault version
  sed -i 's#<param name="revision">.*#<param name="revision">v'"$revWebvault"'</param>#' _service
  sed -i 's#<param name="path">dani-garcia/bw_web_builds/releases/download.*#<param name="path">dani-garcia/bw_web_builds/releases/download/v'"$revWebvault"'/bw_web_v'"$revWebvault"'.tar.gz</param>#' _service
  sed -i 's#DEBTRANSFORM-TAR: bw_web_v.*#DEBTRANSFORM-TAR: bw_web_v'"$revWebvault"'.tar.gz#' vaultwarden-webvault.dsc
fi


#Run local osc services
osc service runall obs_scm
osc service runall set_version
[ "$isVaultwarden" = true ] && osc service runall cargo_vendor


#Update changelog
osc vc
kate ./vaultwarden*.changes ./debian/changelog
read -rsn1 -p"Press any key to continue";echo


#Finish and cleanup
[ -d ./debian ] && tar -czf debian.tar.gz ./debian && rm -rf ./debian
[ -d ./vaultwarden ] && rm -rf ./vaultwarden
[ -d ./bw_web_builds ] && rm -rf ./bw_web_builds
osc commit
osc clean

