#! /bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TEMP_DIR="$( mktemp -d )"

sudo chmod 777 "$TEMP_DIR"
cd "$TEMP_DIR" || exit

sudo apt -qq update && sudo apt -qqy upgrade || exit
sudo apt -qqy install docker.io

sudo service docker start
sudo docker pull vaultwarden/server:latest
sudo docker create --name vw vaultwarden/server:latest
sudo docker cp vw:/vaultwarden /tmp/vaultwarden-resources/
sudo docker rm vw
sudo service docker stop

tar -czvf vaultwarden-bin.tar.gz vaultwarden

cd "$DIR"
[ -f vaultwarden-bin.tar.gz ] && sudo rm vaultwarden-bin.tar.gz
mv "$TEMP_DIR"/vaultwarden-bin.tar.gz ./

sudo rm -rf "$TEMP_DIR"
