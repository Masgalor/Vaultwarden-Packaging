#! /bin/bash

#Get Arguments
while getopts ":r:" opt; do
  case $opt in
    r) RELEASE_TAG=true
    #Don't build the latest commit, but use the latest release-tag instead.
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TEMP_DIR="$( mktemp -d )"
cd "$TEMP_DIR" || exit

#Get the latest project- and compiler-version
revVaultwarden=$(curl -s https://api.github.com/repos/dani-garcia/vaultwarden/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 1-)
revRust=$(curl -s https://api.github.com/repos/rust-lang/rust/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 1-)

sudo apt -qq update && sudo apt -qqy upgrade || exit
sudo apt -qqy install build-essential git pkg-config libssl-dev libsqlite3-dev libmariadb-dev-compat libmariadb-dev libpq-dev
wget https://static.rust-lang.org/dist/rust-"$revRust"-x86_64-unknown-linux-gnu.tar.gz
tar -xf rust-"$revRust"-x86_64-unknown-linux-gnu.tar.gz && cd rust-"$revRust"-x86_64-unknown-linux-gnu
sudo ./install.sh
cd ..

git clone https://github.com/dani-garcia/vaultwarden.git vaultwarden && cd ./vaultwarden
if [ "$RELEASE_TAG" = true ] ; then
  git checkout $(git describe --tags `git rev-list --tags --max-count=1`)
fi

cargo build --features sqlite,mysql,postgresql --release

cd ./target/release
tar -czf ../../../vaultwarden-bin.tar.gz vaultwarden

cd "$DIR"
[ -f vaultwarden-bin.tar.gz ] && sudo rm vaultwarden-bin.tar.gz
mv "$TEMP_DIR"/vaultwarden-bin.tar.gz ./

sudo rm -rf "$TEMP_DIR"
