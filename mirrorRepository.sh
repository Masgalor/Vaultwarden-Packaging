#! /bin/bash

#Get Arguments
while getopts ":r:" opt; do
  case $opt in
    r) RELEASE_TAG=true
    #Don't push the latest commit, but use the latest release-tag instead.
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TEMP_DIR="$( mktemp -d )"

cd "$TEMP_DIR" || exit

git clone git@github.com:dani-garcia/vaultwarden.git vaultwarden && cd ./vaultwarden
if [ "$RELEASE_TAG" = true ] ; then
  git checkout $(git describe --tags `git rev-list --tags --max-count=1`)
fi
git push git@codeberg.org:Masgalor/Vaultwarden.git
git push --tags git@codeberg.org:Masgalor/Vaultwarden.git

git clone git@github.com:dani-garcia/bw_web_builds.git vaultwarden-webvault && cd ./vaultwarden-webvault
if [ "$RELEASE_TAG" = true ] ; then
  git checkout $(git describe --tags `git rev-list --tags --max-count=1`)
fi
git push git@codeberg.org:Masgalor/Vaultwarden-WebVault.git
git push --tags git@codeberg.org:Masgalor/Vaultwarden-WebVault.git

rm -rf "$TEMP_DIR"
